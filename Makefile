CC=gcc
NCURSES_PARAM=-I$(HOME)/include/ncurses -L$(HOME)/lib

all: test_000 test_001 example_14 example_15 example_18

#	Uso la lib ncurses installata nella home del mio utente

test_000: main_000.c
	$(CC) -o bin/$@ $(NCURSES_PARAM) -lncurses $^

test_001: main_001.c
	$(CC) -o bin/$@ $(NCURSES_PARAM) -lncurses $^

example_14: example_14.c
	$(CC) -o bin/$@ $(NCURSES_PARAM) -lpanel -lncurses $^

example_15: example_15.c
	$(CC) -o bin/$@ $(NCURSES_PARAM) -lpanel -lncurses $^

example_18: example_18.c
	$(CC) -o bin/$@ $(NCURSES_PARAM) -lmenu  -lncurses $^

clean:
	rm -vf *.o bin/*
