#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>

int main( char *argc, char** argv ) {
  WINDOW *win;

  initscr();
  
  printw( "Premi un tasto" );
  refresh();
  getchar();

  win = newwin( 20, 20, 0, 0 );

  box( win, 0 , 0 );  
  wrefresh( win );

  getchar();

  delwin( win );

  endwin();

  return 0;
}
